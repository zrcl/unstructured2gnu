 #uncomment the first two lines to export to latex
 #set terminal cairolatex
 #set output 'fig.tex'

set terminal png size 600,600
set output "example.png"

 set xrange [   0.0000000000000000      :   1.0000000000000000      ]
 set yrange [   0.0000000000000000      :   1.0000000000000000      ]
 set size ratio    1.0000000000000000
 fname ='generated.dat'
 ### 2 PLOT OPTIONS:
 # simpler option givin a pixelly look
 set pm3d map
 plot fname u(   4.1666666666666666E-003 +$1*   8.3333333333333332E-003 ):(   4.1666666666666666E-003 +$2*   8.3333333333333332E-003 ):3 matrix with image notitle
 # more elaborate option, can be a bit longer, with interpolation.
 #set pm3d interpolate 2,2 #2,2 controls the interpolation," replace by 0,0 to let gnuplot chose the interpolation order
 #splot fname u(   4.1666666666666666E-003 +$1*   8.3333333333333332E-003 ):(   4.1666666666666666E-003 +$2*   8.3333333333333332E-003 ):3 matrix
