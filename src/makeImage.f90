! unstructured2gnu
! Copyright (C) 2020  Christophe Langlois

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
program main
  implicit none
  integer(kind=4)  :: nlines
  real(kind=8) ,allocatable,dimension(:) :: x,y,z
  integer(kind=4)  :: ii,jj     !counter
  real(kind=8)  :: xmin,xmax,ymin,ymax,zmin,zmax
  integer(kind=4)  :: xres,yres,nSkippedLines
  real(kind=8)  :: xstep,ystep,zero
  integer(kind=4)  :: line,col
  real(kind=8)  :: xyRatio


  real(kind=8), allocatable,dimension(:,:)  :: cumSum,image
  integer(kind=8), allocatable,dimension(:,:) :: nValues

  character(len=2048) :: fileIn,fileOut
  
  print *,'resolution along x (integer) :'
  read *, xres
  print *,'resolution along y (integer) :'
  read *, yres
  ! xres=120
  ! yres=120
  allocate(cumSum(xres,yres))
  allocate(nValues(xres,yres))
  allocate(image(xres,yres))

  print *,'Input file name (include extention) :'
  read *,fileIn
  print *,'How many lines should I skip at the begining ?'
  read *,nSkippedLines
  print *,'Output file name (include extention) :'
  read *,fileOut
  ! fileIn='unstructured.dat'
  ! fileout='out.dat'
  nlines = countLines(trim(fileIn))
  nlines = nlines - nSkippedLines
  allocate(x(nlines))
  allocate(y(nlines))
  allocate(z(nlines))
  open(10,file=trim(fileIn))
  if (nSkippedLines.gt.0) then
     do ii=1,nSkippedLines
        read(10,*)
     end do
  end if
  do ii=1,nlines
     read(10,*) x(ii),y(ii),z(ii)
  end do
  xmin=minval(x)
  xmax=maxval(x)
  ymin=minval(y)
  ymax=maxval(y)
  zmin=minval(z)
  zmax=maxval(z)
  xyRatio=(ymax-ymin)/(xmax-xmin)
  !offset so all coordinates are positive
  xstep=(xmax-xmin)/xres
  ystep=(ymax-ymin)/yres

  x=x-xmin
  y=y-ymin
  
  cumSum=0
  nValues=0
  do ii=1,nlines
     line=ceiling(x(ii)/xstep)
     if (line.eq.0) then
        line = 1
     end if
     col =ceiling(y(ii)/ystep)
     if (col.eq.0) then
        col = 1
     end if
     cumSum(line,col) = cumSum(line,col) + z(ii)
     nValues(line,col)= nValues(line,col) + 1
  end do
  ! if (any(nValues.eq.0)) then
  !    stop 'resolution finer than data entry, NO FILES GENERATED'
  ! end if

  ! I know i might divide by zero but this allows to get non-rectangular fiels
  ! (NaN or Inf won't be ploted by gnuplot)
  image = cumSum/nValues

  open(11,file=fileOut)
  do ii=1,yres
     write(11,*) image(:,ii)
  end do
  close(11)
  deallocate(x)
  deallocate(y)
  deallocate(z)
  deallocate(cumSum)
  deallocate(nValues)
  deallocate(image)

  open(12,file='plt.gp')
  write(12,*) "#uncomment the first two lines to export to latex"
  write(12,*) "#set terminal cairolatex"
  write(12,*) "#set output 'fig.tex'"
  write(12,*) "set xrange [",xmin,":",xmax,"]"
  write(12,*) "set yrange [",ymin,":",ymax,"]"
  write(12,*) "set size ratio ",xyRatio
  write(12,*) "fname ='",trim(fileout),"'"
  write(12,*) "### 2 PLOT OPTIONS:"
  write(12,*) "# simpler option givin a pixelly look"
  write(12,*) "plot fname u(",xmin + (xstep)/2,"+$1*",xstep,"):(",ymin+ystep/2,"+$2*",ystep,"):3 matrix with image"
  write(12,*) "#"
  write(12,*) "# more elaborate option, can be a bit longer, with interpolation."
  write(12,*) "#set pm3d map"
  write(12,*) "#set pm3d interpolate 2,2 #2,2 controls the interpolation,"&
       " replace by 0,0 to let gnuplot chose the interpolation order"
  write(12,*) "#splot fname u(",xmin + (xstep)/2,"+$1*",xstep,"):(",ymin+ystep/2,"+$2*",ystep,"):3 matrix"
  close(12)
  
  

contains
  function countLines(fileName) result(nLines)
    implicit none
    character(len=*) ::fileName
    integer(kind=4)  :: nLines,io

    open(10,file=fileName,iostat=io,status='old')
    if (io/=0) stop 'cant open file'

    nlines = 0
    do
       read(10,*,iostat=io)
       if (io/=0)exit
       nlines=nlines+1
    end do
    close(10)
  end function countLines
end program main
