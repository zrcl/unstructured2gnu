# unstructured2gnu
# Copyright (C) 2020  Christophe Langlois

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
install:
	gfortran -O3 src/makeImage.f90 -o src/makeImageForotran
	cp src/unstructured2im.def unstructured2gnu
	@echo "printf \"%s\n\" \$$xres \$$yres \$$inFile \$$nskip \$$outFile | "$(shell pwd)"/src/makeImageForotran >> /dev/null" >> unstructured2gnu
	sudo chmod +x unstructured2gnu
	sudo cp unstructured2gnu /usr/bin/unstructured2gnu
clean:
	sudo rm /usr/bin/unstructured2gnu
